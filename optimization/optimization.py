"""MC1-P2: Optimize a portfolio.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988

Note:  99%+ of code below is from a previous semester attempt.
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data
from scipy.optimize import minimize


def compute_portfolio_stats(allocs, prices, sv=1.0, rfr=0.0, sf=252.0):
    """
    Returns portfolio statistics that are commonly used to assess a portfolio.

    Note:  When we compute statistics on the portfolio value, we do not include the first day.

    :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
    :param prices:
    :param sv: Start value of the portfolio
    :param rfr: The risk free return per sample period that does not change for the entire date range (a single number, not an array).
    :param sf: Sampling frequency per year.  Number of days traded assumed to be 252 days out of the year.
    :return:
    """

    normalized_prices = prices * allocs * sv
    portfolio_value = normalized_prices.sum(axis=1)
    change_in_value = portfolio_value.pct_change()
    cr = (portfolio_value.iloc[-1] - sv) / sv
    adr = change_in_value[1:].mean()
    sddr = change_in_value[1:].std()
    sr = sharpe_ratio(prices, allocs)

    return cr, adr, sddr, sr, portfolio_value


def sharpe_ratio(prices, allocs, sv=1.0, rfr=0.0, sf=252.0):
    """
    Used to compute the Sharpe Ratio.  This method utilizes the 'traditional shortcut' from Udacity lecture vids.
    :param prices:
    :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
    :param sv: Share Value of a particular stock
    :param rfr: Rate-Free Return assumed to be zero.
    :param sf: Number of days traded assumed to be 252 days out of the year.
    :return:
    """

    normalized_prices = prices * allocs * sv
    portfolio_value = normalized_prices.sum(axis=1)
    change_in_value = portfolio_value.pct_change()
    sr = np.power(1, (1 / sf)) * (change_in_value[1:] - rfr).mean() / ((change_in_value[1:]).std())

    return sr


def run_sharpe_ratio(allocs, prices):
    """
    Used to minimize the Sharpe Ratio. :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the
    allocations must be between 0.0 and 1.0 and they must sum to 1.0. :param prices: :return: Sharp ratio
    """
    return -1.0 * sharpe_ratio(allocs, prices)


# This is the function that will be tested by the autograder  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008, 1, 1), ed=dt.datetime(2009, 1, 1), syms=['GOOG', 'AAPL', 'GLD', 'XOM'],
                       gen_plot=False):
    """
    Find allocations to the symbols that optimize the criteria given above. Assume 252 trading days in a year and a risk
    free return of 0.0 per day.

    * Read in adjusted closing prices for the equities.
    * Normalize the prices according to the first day. The first row for each stock should have a value of 1.0 at this point.
    * Multiply each column by the allocation to the corresponding equity.
    * Multiply these normalized allocations by starting value of overall portfolio, to get position values.
    * Sum each row (i.e. all position values for each day). That is your daily portfolio value.
    * Compute statistics from the total portfolio value.

    Args:
        sd: A datetime object that represents the start date
        ed: A datetime object that represents the end date
        syms:  A list of 2 or more symbols that make up the portfolio (note that your code should support any symbol in the data directory)
        gen_plot: If False, do not create any output. If True it is OK to output a plot such as plot.png

    Returns:
        allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
        cr: Cumulative return
        adr: Average daily return
        sddr: Standard deviation of daily return
        sr: Sharpe ratio
    """

    # read in adjusted closing prices for the equities
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # normalize prices according to the first day.  The first row for each stock should have a value of 1.0.
    prices /= prices.iloc[0, :]
    num_allocs = len(syms)  # number of stocks to allocate for

    # uniform allocation of 1/n to each of the n assets as your initial guess.
    alloc_percent = float(1.0 / num_allocs)
    allocs = np.zeros(num_allocs)
    for i in range(num_allocs):
        allocs[i] = alloc_percent

    # setup minimizer with the following parameters to minimize the Sharpe Ratio
    constraints = ({'type': 'eq', 'fun': lambda x: 1 - sum(x)})
    bounds = tuple((0, 1) for _ in range(num_allocs))
    minimized_result = minimize(run_sharpe_ratio, allocs, args=(prices,), method="SLSQP", bounds=bounds,
                                constraints=constraints)
    allocs = minimized_result.x

    # common portfolio statistics measured from portfolio
    cr, adr, sddr, sr, port_val = compute_portfolio_stats(prices, allocs)

    # Compare daily portfolio value with SPY using a normalized plot  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    if gen_plot:
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)
        df_temp = df_temp / df_temp.iloc[0, :]  # normalize prices
        df_temp.plot(title="Daily Portfolio Value and SPY")
        plt.xlabel("Date")
        plt.ylabel("Price")
        plt.savefig('comparison_optimal.png')

    return allocs, cr, adr, sddr, sr


def portfolio_stats_report():
    """
    Your report as report.pdf that includes a chart comparing the optimal portfolio with SPY using the
    following parameters:
      Start Date: 2008-06-01,
      End Date: 2009-06-01,
      Symbols: ['IBM', 'X', 'GLD', 'JPM'].
    :return:
    """

    start_date = dt.datetime(2008, 6, 1)
    end_date = dt.datetime(2009, 6, 1)
    symbols = ['IBM', 'X', 'GLD', 'JPM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd=start_date, ed=end_date,
                                                        syms=symbols,
                                                        gen_plot=True)

    # Print statistics
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    print(f"Symbols: {symbols}")
    print(f"Allocations:{allocations}")
    print(f"Sharpe Ratio: {sr}")
    print(f"Volatility (stdev of daily returns): {sddr}")
    print(f"Average Daily Return: {adr}")
    print(f"Cumulative Return: {cr}")


def portfolio_stats_one():
    """
    Example 1 to check work from Assess Portfolio project.

    Start Date: 2010-01-01
    End Date: 2010-12-31
    Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']
    Allocations: [0.2, 0.3, 0.4, 0.1]
    Sharpe Ratio: 1.51819243641
    Volatility (stdev of daily returns): 0.0100104028
    Average Daily Return: 0.000957366234238
    Cumulative Return: 0.255646784534
    :return:
    """

    start_date = dt.datetime(2010, 1, 1)
    end_date = dt.datetime(2010, 12, 31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd=start_date, ed=end_date,
                                                        syms=symbols,
                                                        gen_plot=True)

    # Print statistics
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    print(f"Symbols: {symbols}")
    print(f"Allocations:{allocations}")
    print(f"Sharpe Ratio: {sr}")
    print(f"Volatility (stdev of daily returns): {sddr}")
    print(f"Average Daily Return: {adr}")
    print(f"Cumulative Return: {cr}")


def portfolio_stats_two():
    """
    Example 2 to check work from Assess Portfolio project.

    Start Date: 2010-01-01
    End Date: 2010-12-31
    Symbols: ['AXP', 'HPQ', 'IBM', 'HNZ']
    Allocations: [0.0, 0.0, 0.0, 1.0]
    Sharpe Ratio: 1.30798398744
    Volatility (stdev of daily returns): 0.00926153128768
    Average Daily Return: 0.000763106152672
    Cumulative Return: 0.198105963655
    :return:
    """

    start_date = dt.datetime(2010, 1, 1)
    end_date = dt.datetime(2010, 12, 31)
    symbols = ['AXP', 'HPQ', 'IBM', 'HNZ']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd=start_date, ed=end_date,
                                                        syms=symbols, gen_plot=True)

    # Print statistics
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    print(f"Symbols: {symbols}")
    print(f"Allocations:{allocations}")
    print(f"Sharpe Ratio: {sr}")
    print(f"Volatility (stdev of daily returns): {sddr}")
    print(f"Average Daily Return: {adr}")
    print(f"Cumulative Return: {cr}")


def portfolio_stats_three():
    """
    Example 3 to check work from Assess Portfolio project.

    Start Date: 2010-06-01
    End Date: 2010-12-31
    Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']
    Allocations: [0.2, 0.3, 0.4, 0.1]
    Sharpe Ratio: 2.21259766672
    Volatility (stdev of daily returns): 0.00929734619707
    Average Daily Return: 0.00129586924366
    Cumulative Return: 0.205113938792

    :return:
    """

    start_date = dt.datetime(2010, 6, 1)
    end_date = dt.datetime(2010, 12, 31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd=start_date, ed=end_date,
                                                        syms=symbols, gen_plot=True)

    # Print statistics
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    print(f"Symbols: {symbols}")
    print(f"Allocations:{allocations}")
    print(f"Sharpe Ratio: {sr}")
    print(f"Volatility (stdev of daily returns): {sddr}")
    print(f"Average Daily Return: {adr}")
    print(f"Cumulative Return: {cr}")


def portfolio_stats_four():
    """
    Tries to imitate numbers from Chart Example:  http://quantsoftware.gatech.edu/Spring_2020_Project_2:_Optimize_Something

    Start Date: 2010-06-01
    End Date: 2010-12-31
    Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']
    Allocations: [0.2, 0.3, 0.4, 0.1]
    Sharpe Ratio: 2.21259766672
    Volatility (stdev of daily returns): 0.00929734619707
    Average Daily Return: 0.00129586924366
    Cumulative Return: 0.205113938792

    :return:
    """

    start_date = dt.datetime(2010, 1, 1)
    end_date = dt.datetime(2010, 12, 31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd=start_date, ed=end_date,
                                                        syms=symbols, gen_plot=True)

    # Print statistics
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    print(f"Symbols: {symbols}")
    print(f"Allocations:{allocations}")
    print(f"Sharpe Ratio: {sr}")
    print(f"Volatility (stdev of daily returns): {sddr}")
    print(f"Average Daily Return: {adr}")
    print(f"Cumulative Return: {cr}")


def test_code():
    """
    Your report as report.pdf that includes a chart comparing the optimal portfolio with SPY using the following parameters:
    Start Date: 2008-06-01, End Date: 2009-06-01, Symbols: ['IBM', 'X', 'GLD', 'JPM'].

    Returns:

    """

    # This function WILL NOT be called by the auto grader  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Do not assume that any variables defined here are available to your function/code  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # It is only here to help you set up and test your code  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			

    # Define input parameters  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Note that ALL of these values will be set to different values by  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # the autograder!

    # portfolio_stats_one() #Assess Portfolio Example 1
    # portfolio_stats_two() #Assess Portfolio Example 2
    # portfolio_stats_three() #Assess Portfolio Example 3
    # portfolio_stats_four() #Optimize Something Chart Example
    portfolio_stats_report()  # Chart to place into report.pdf


if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Do not assume that it will be called  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    test_code()
